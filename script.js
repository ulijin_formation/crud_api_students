
//Gestion de la soumission du formulaire
let selectedRow = null;
function onFormSubmit(e) {

    let formData = readFormData();
    if (selectedRow === null) {
        newEntry(formData);
    }
    else {
        updateRecord(formData);

    }
    resetForm();

}

//Fonction pour Récupérer les données
function readFormData() {
    let formData = {};
    formData["id"] = document.getElementById("id").value;
    formData["lastName"] = document.getElementById("lastName").value;
    formData["firstName"] = document.getElementById("firstName").value;
    formData["birthDate"] = document.getElementById("birthDate").value;
    formData["idCard"] = document.getElementById("idCard").value;
    return formData;
}

//Fonction pour inserer les données dans le tableau
function newEntry(data) {
    fetch('http://localhost:3000/student', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(result => {
            let table = document.getElementById("studentTable").getElementsByTagName('tbody')[0];
            let newRow = table.insertRow(table.length);
            /*méthode qui insère une nouvelle cellule td dans une ligne de tableau tr et renvoie une référence sur cette cellule */
            let cell1 = newRow.insertCell(0);
            cell1.innerHTML = data.lastName;
            let cell2 = newRow.insertCell(1);
            cell2.innerHTML = data.firstName;
            let cell3 = newRow.insertCell(2);
            cell3.innerHTML = data.birthDate;
            let cell4 = newRow.insertCell(3);
            cell4.innerHTML = calculAge(data.birthDate);
            let cell5 = newRow.insertCell(4);
            cell5.innerHTML = data.idCard;
            let cell6 = newRow.insertCell(5);
            cell6.innerHTML = '<button onClick=\'onEdit(this)\'><i class="fa fa-edit"></i><span class="tooltip">Modifier</span></button> <button onClick=\'onDelete(this)\'><i class="fa fa-trash"></i><span class="tooltip">Supprimer</span></button>';

        });
}

//Fonction pour modifier les données via le formulaire
function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById('id').value = selectedRow.cells[5].innerHTML;
    document.getElementById('lastName').value = selectedRow.cells[0].innerHTML;
    document.getElementById('firstName').value = selectedRow.cells[1].innerHTML;
    document.getElementById('birthDate').value = selectedRow.cells[2].innerHTML;
    document.getElementById('idCard').value = selectedRow.cells[4].innerHTML;
}

//Fonction pour Enregistrer les nouvelles données saisies
function updateRecord(formData) {
    fetch('http://localhost:3000/students/' + formData.id, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Echec de la mise à jour');
            }
            return response.json();
        })
        .then(() => {
            if (selectedRow !== null) {
                selectedRow.cells[0].innerHTML = formData.lastName;
                selectedRow.cells[1].innerHTML = formData.firstName;
                selectedRow.cells[2].innerHTML = formData.birthDate;
                selectedRow.cells[4].innerHTML = formData.idCard;
            }
            resetForm();
            return fetchAndDisplayStudents();
        })
        .catch(error => console.error(error));
}

//Fonction pour supprimer les données du tableau
function onDelete(td) {
    let row = td.parentElement.parentElement;
    let id = row.cells[5].innerHTML;
    fetch('http://localhost:3000/student/' + id, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Echec de la suppression');
            }
            row.remove();
        })
        .catch(error => console.error(error));
}

//Fonction pour effacerles données
function resetForm() {

    document.getElementById('lastName').value = '';
    document.getElementById('firstName').value = '';
    document.getElementById('birthDate').value = '';
    document.getElementById('idCard').value = '';
    selectedRow = null;
}
//Fonction pour calculer l'age 
function calculAge(dtn) {

    let today = new Date();
    let birthDate = new Date(dtn);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }
    return age;
}
//Tri par ordre croissant ou décroissant

function sortTable(c, type) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("studentTable");
    switching = true;

    //Ordre croissant
    if (type == 1) {
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[c];
                y = rows[i + 1].getElementsByTagName("TD")[c];
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;

            }
        }
    }
    //Ordre décroissant
    if (type == 0) {
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[c];
                y = rows[i + 1].getElementsByTagName("TD")[c];
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;

            }
        }
    }
}
// Déclaration de l'URL de l'API
const apiUrl = "http://localhost:3000";

const studentTable = document.getElementById("studentTable");

// Fonction pour prendre les étudiants de l'API
async function getStudentsFromAPI() {
    try {
        const response = await fetch(apiUrl + "/students");
        return response.json();
    } catch (error) {
        console.error("Error while fetching students from API:", error);
        return null;
    }
}
// Fonction pour afficher les étudiants dans la table
function displayStudentsInTable(students) {
    studentTable.innerHTML = `
    <tr>
      <th>Nom</th>
      <th>Prénom</th>
      <th>Date de naissance</th>
      <th>Age</th>
      <th>Numéro de carte d'identité</th>
      <th>ID étudiant</th>
      <th>Actions</th>
    </tr>
  `;
    // Boucle pour afficher les informations des étudiants
    for (const student of students.students) {
        studentTable.innerHTML += `
      <tr>
        <td>${student.lName}</td>
        <td>${student.fName}</td>
        <td>${student.bDate}</td>
        <td>${calculAge(student.bDate)}</td>
        <td>${student.iCard}</td>
        <td>${student.idStudents}</td>
        <td>
        <button onClick=\'onEdit(this)\'><i class="fa fa-edit"></i><span class="tooltip">Modifier</span></button>
         <button onClick=\'onDelete(this)\'><i class="fa fa-trash"></i><span class="tooltip">Supprimer</span></button>
        </td>
      </tr>
    `;
    }
}
// Fonction pour récupérer et afficher les étudiants
async function fetchAndDisplayStudents() {
    // Vérification de la présence des étudiants dans le stockage local
    let students = localStorage.getItem("students");
    if (students) {
        students = JSON.parse(students);
        displayStudentsInTable(students);
    }
    // Récupération des étudiants de l'API
    const studentsFromAPI = await getStudentsFromAPI();
    if (studentsFromAPI) {
        students = studentsFromAPI;
        localStorage.setItem("students", JSON.stringify(students));
        displayStudentsInTable(students);
    }
}
//Appelle la fonction fetchAndDisplayStudents()pour l'exécuter au chargement de la page.

fetchAndDisplayStudents();
