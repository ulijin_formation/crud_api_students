const express = require("express");
const path = require("path");
const sqlite3 = require("sqlite3").verbose();
const bodyParser = require('body-parser');
const cors = require('cors');
// Création de l'application express
const app = express();

// Autorisation du traitement des données envoyées en format JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//Cors
app.use(cors({
    origin: function(origin, callback){
        console.log('ORIGIN ', origin)
        if(!origin) return callback(null, true);
        return callback(null, true);
      }
    }));


// Chemin vers la base de données
const dbname = path.join(__dirname, "data", "apptest.db");

// Fonction pour se connecter à la base de données
const connectDB = () =>
    new Promise((resolve, reject) => {
        let db = new sqlite3.Database(dbname, err => {
            if (err) {
                reject(err);
            }
            console.log("Connecté à 'apptest.db'");
            resolve(db);
        });
    });

// Route pour la page d'accueil
app.get("/", (req, res) => {
    res.json({ message: "Accueil" });
});

// Route pour récupérer la liste des étudiants
app.get("/students", async (req, res) => {
    try {
        const db = await connectDB();
        const sql = "SELECT * FROM Students";
        const rows = await new Promise((resolve, reject) => {
            db.all(sql, [], (err, rows) => {
                if (err) {
                    reject(err);
                }
                resolve(rows);
            });
        });
        db.close();
        res.json({ students: rows });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err.message });
    }
});
//calcule de l'age
function calculAge(dtn) {
   
    let today = new Date();
    let birthDate = new Date(dtn);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }
    return age;
}
// Route pour ajouter un étudiant
app.post("/student", async (req, res) => {
    try {
        const db = await connectDB();
        const sql = "INSERT INTO Students(lName, fName, bDate, iCard) VALUES (?,?,?,?)";
        const params = [req.body.lastName, req.body.firstName, req.body.birthDate, req.body.idCard];
        await new Promise((resolve, reject) => {
            db.run(sql, params, err => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
        db.close();
        res.json({ message: "Ajout réussi" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err.message });
    }
});
// Route pour mettre à jour un étudiant
app.put("/students/:id", async (req, res) => {
    try {
        const db = await connectDB();
        const sql = "UPDATE Students SET lName = ?, fName = ?, bDate = ?, iCard = ? WHERE idStudents = ?";
        const params = [req.body.lastName, req.body.firstName, req.body.birthDate, req.body.idCard, req.params.id];
        await new Promise((resolve, reject) => {
            db.run(sql, params, err => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
        db.close();
        res.json({ message: "Mise à jour réussie" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err.message });
    }
});

//Route pour supprimer un étudiant
app.delete("/student/:id", async (req, res) => {
    try {
        const db = await connectDB();
        const sql = "DELETE FROM Students WHERE idStudents = ?";
        const params = [req.params.id];
        await new Promise((resolve, reject) => {
            db.run(sql, params, err => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
        db.close();
        res.json({ message: "Suppression réussi" });
    } catch (err) {
      
        res.status(500).json({ error: err.message });
    }
});
// Démarrage du serveur sur le port 3000
app.listen(3000, () => {
    console.log("Server started on port 3000");
});
